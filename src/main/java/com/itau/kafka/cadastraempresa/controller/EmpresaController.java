package com.itau.kafka.cadastraempresa.controller;

import com.itau.kafka.cadastraempresa.producer.EmpresaProducer;
import com.itau.kafka.cadastraempresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmpresaController {

    @Autowired
    private EmpresaProducer empresaProducer;

    @PostMapping("/empresa")
    public void create(@RequestBody Empresa empresa) {
        empresaProducer.enviarAoKafka(empresa);
    }

}
