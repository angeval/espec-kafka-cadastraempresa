package com.itau.kafka.cadastraempresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastraempresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastraempresaApplication.class, args);
	}

}
