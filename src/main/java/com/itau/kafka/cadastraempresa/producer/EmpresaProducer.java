package com.itau.kafka.cadastraempresa.producer;

import com.itau.kafka.cadastraempresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec2-angela-valentim-2", empresa);
    }

}